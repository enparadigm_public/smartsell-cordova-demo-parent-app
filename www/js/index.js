/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        console.log('Received Event: ' + id);
        document.getElementById("init").addEventListener("click", isUserInitialised);

        console.log('Received Event: ' + id);
    }
};

app.initialize();

function init() {
    document.getElementById("result_init").innerHTML = " Init Clicked";
    var plugin = new SmartSellPlugin();
    var base_url = "https://dev.enparadigm.com/starhealth_dev_api/public/index.php/v1/";
    var mobile = "9999999999"
    var url = base_url + "login";

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var response = JSON.parse(this.responseText);
    plugin.initialise(base_url,
        response.user.username,"UsersName","email@mail.com","Developer","en","1",response.token.access_token,response.token.refresh_token,function (msg) {
        isInitialized = true;
        document.getElementById("result_init").innerHTML = " Init Success";
        setTimeout(
            function () {
                document.getElementById("result_init").innerHTML = "";
            }, 5000);
    },
        function (err) {
            isInitialized = false;
            document.getElementById("result_init").innerHTML = " Init Failed";
        });
    }
  };
  xhttp.open("POST", url, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("username="+mobile+"&user_type_id=1");
  
}

function isUserInitialised() {
    var plugin = new SmartSellPlugin();
    plugin.isUserInitialised("9999999999",function (msg) {
        document.getElementById("result_init").innerHTML = " isUserInitialised Success";
    },
        function (err) {
            document.getElementById("result_init").innerHTML = " isUserInitialised Failed";
            console.log(err);
            init();
        });
}

